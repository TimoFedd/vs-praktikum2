-module(node).
-export([loop/12]).
																												
loop(BasicEdges,BranchEdges,RejectEdges,Status,BestEdge,Level,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge)->
	
	receive
		
%%---------------------------wakeup----------------------------------------------------------------------		
		
		{wakeup} ->
			
			if Status == sleeping ->
	           werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Node ist aufgewacht \r\n"),
			   {NewBasicEdges,NewBranchEdges,NewLevel,NewStatus} = wakeup(BasicEdges,MyNodeName),
			   
			   loop(NewBasicEdges,NewBranchEdges,RejectEdges,NewStatus,BestEdge,NewLevel,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge);   
			true->%else
       		   true
			end;
	
%%---------------------------connect---------------------------------------------------------------------------   
		
		{connect,L,Edge} ->
			
			%X und Y vertauscht so das X der eigene Knoten ist und Y der knoten von dem die Nachricht gekommen ist
			{Weight,NodeY,NodeX} = Edge,

			%werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Empfangen: 'Connect' von "++werkzeug:to_String(NodeY)++"\r\n"),
			
			if Status == sleeping ->
				
				{NewBasicEdges,NewBranchEdges,NewLevel,NewStatus} = wakeup(BasicEdges,MyNodeName);
				   
			%else
			Status /= sleeping  ->
		 
		         NewLevel = Level,
		         NewStatus = Status,
				 NewBranchEdges =BranchEdges,
                 NewBasicEdges= BasicEdges
              end,
			
			
			
			  
				 if L < NewLevel ->
					  werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Empfangen: 'Connect' von "++werkzeug:to_String(NodeY)++"\r\n"),
				      %sendet an den Knoten der das 'Connect' gesenden hat, ein initiate zur�ck
			          NID =global:whereis_name(NodeY),
			          NID !{initiate,NewLevel,FragName,NewStatus,{Weight,NodeX,NodeY}},
				      werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Gesendet: 'Initiate' an Node: "++werkzeug:to_String(NodeY)++"\r\n"),
			          
				          if NewStatus == find ->
					    	NewFindCount =  FindCount +1;
					      true->
							NewFindCount =  FindCount
						  end,
				 
					 %Pr�fen ob es nicht schon eine Branch Kante ist, was sein kann wenn dies schon durch die Prozedur changeRoot geschehen ist
                     %Und dann nur hinzuf�gen wenn es noch keine ist 
					  IsBasic = edgeManagment:isBasicEdge(Weight,NewBranchEdges),  
                      if IsBasic == false ->
					        [NBasic,NBranch] =  edgeManagment:basicToBrunchEdge(Weight,NewBasicEdges,NewBranchEdges); 
						 IsBasic == true -> %else
							NBasic = NewBasicEdges,
							NBranch = NewBranchEdges
					  end,
               	werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"),werkzeug:timeMilliSecond()++" Der Knoten "++werkzeug:to_String(MyNodeName)++" hat nun vollgende Branch Kanten: "++werkzeug:to_String(NBranch)++"\r\n"),
				
			          loop(NBasic,NBranch,RejectEdges,NewStatus,BestEdge,NewLevel,MyNodeName,FragName,InBranch,NewFindCount,BestWT,TestEdge);   
				  
			      true->%else
				          %Wenn die Kante eine Basis Kante ist, wird die connect Nachricht delayed
				          IsBasic = edgeManagment:isBasicEdge(Weight,NewBasicEdges),
                          if IsBasic == true ->   
				       true,
					      %Die Nachricht neu an sich selbst senden
					     self()! {connect,L,Edge},  %Zeitverz�gert da der Prozess sich sonst zu schnell zu viele Nachrichten schicken w�rde
					      loop(NewBasicEdges,NewBranchEdges,RejectEdges,NewStatus,BestEdge,NewLevel,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge);
					   
					   true -> %else
                          werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Empfangen: 'Connect' von "++werkzeug:to_String(NodeY)++"\r\n"),
                          %sendet an den Knoten der das 'Connect' gesenden hat, ein initiate zur�ck
					      NID =global:whereis_name(NodeY),
			              NID !{initiate,NewLevel+1,Weight,find,{Weight,NodeX,NodeY}},
				          werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Gesendet: 'Initiate' an Node: "++werkzeug:to_String(NodeY)++"\r\n"),
				         loop(NewBasicEdges,NewBranchEdges,RejectEdges,NewStatus,BestEdge,NewLevel,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge)
                       end
	
				 
				  end;
			
			
			
%%---------------------------initiate-------------------------------------	   				   
            
	
	    {initiate,L,F,S,Edge} ->
                 
       %Die Kante von der das Initiate kommt, wird zur InBranch Kante
	   {InBranchWeight,NodeY,NodeX} = Edge,
	   NewInBranchEdge = {InBranchWeight,NodeX,NodeY},
  
       NewBestWT= 99999, %wird bei jedem Aufruf von initiate auf unendlich gesetzt
	   NewBestEdge = {},  %wird bei jedem Aufruf von initiate auf {} gesetzt
	  
       werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"),werkzeug:timeMilliSecond()++" Empfangen: 'initiate' vom Knoten "++werkzeug:to_String(NodeY)++"\r\n"),  
	   werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"),werkzeug:timeMilliSecond()++" Der Knoten "++werkzeug:to_String(NodeX)++" hat nun vollgenden Status-Level-Fragmentnamen: "++werkzeug:to_String(S)++"-"++werkzeug:to_String(L)++"-"++werkzeug:to_String(F)++"\r\n"), 

 
       %sendet an alle brunch Kanten exklusive der InBranch, ein Initiate, und erh�ht gegbenenfalls den find Counter
	   AddFindCount = edgeManagment:initToBrunch(NodeX,BranchEdges,InBranchWeight,FindCount,L,S,F,MyNodeName),
	   
	   	   %Wenn der Status 'find' ist, Prozedur test() aufrufen, sonst nicht
	       if S == find ->
			   Gesamtzustand = {BasicEdges,BranchEdges,RejectEdges,S,NewBestEdge,L,MyNodeName,F, NewInBranchEdge,FindCount+AddFindCount,NewBestWT,TestEdge},
		       NewTestEdge = test(BasicEdges,L,F,NodeX,FindCount+AddFindCount,NewBestEdge,MyNodeName,Gesamtzustand );
	          true->																						
		       NewTestEdge = TestEdge
	       end,

	   loop(BasicEdges,BranchEdges,RejectEdges,S,NewBestEdge,L,MyNodeName,F, NewInBranchEdge,FindCount+AddFindCount,NewBestWT, NewTestEdge);
            

%%---------------------------test----------------------------------------		
		
        {test,L,F,Edge} ->
		
	    %X und Y vertauscht so das X der eigene Knoten ist und Y der knoten von dem die Nachricht gekommen ist
	    {Weight,NodeY,NodeX} = Edge,
		

         	if Status == sleeping ->
				{NewBasicEdges,NewBranchEdges,NewLevel,NewStatus} = wakeup(BasicEdges,MyNodeName);
			  
			%else
			Status /= sleeping  ->
		 
		         NewLevel = Level,
		         NewStatus = Status,
				 NewBranchEdges =BranchEdges,
                 NewBasicEdges= BasicEdges
              end,

              
                %Wenn das Level des anfragenden Knoten gr��er ist, wird die Nachricht zum warten wieder an mich selber gesendet
 				if L > NewLevel ->
					   timer:send_after(100,self(),{test,L,F,Edge});  %Zeitverz�gert da der Prozess sich sonst zu schnell zu viele Nachrichten schicken w�rde
				true-> %else
					  if F /= FragName ->  %wenn der Fragname untersschiedlich ist, sende ein Accept zur�ck
					         NID =global:whereis_name(NodeY),
			                 NID !{accept,{Weight,NodeX,NodeY}} ,
		 					 werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Gesendet: 'Accept' an Node: "++werkzeug:to_String(NodeY)++"\r\n");
		  			   true->%else
                              
					             IsBasic = edgeManagment:isBasicEdge(Weight,NewBasicEdges),
                                 
						         %Wenn die Kante eine Basic kante ist, wird sie als Rejectet Kante markiert
						         if  IsBasic == true ->   
                                    [NBasic,NReject] =  edgeManagment:basicToRejectEdge(Weight,NewBasicEdges,RejectEdges);

						         IsBasic == false -> %else nix
                                      NBasic =  NewBasicEdges,
								      NReject = RejectEdges
 							     end,


								  %Wenn die Kante ungleich der testEdge ist, sende ein Reject zur�ck, ansonsten f�hre die Prozedur test () aus
								 if TestEdge /= {Weight,NodeX,NodeY} ->
								    NID =global:whereis_name(NodeY),
			                        NID !{reject,{Weight,NodeX,NodeY}} ,
				  				    werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Gesendet: 'reject' an Node: "++werkzeug:to_String(NodeY)++"\r\n"),
				 				    loop(NBasic,NewBranchEdges,NReject,NewStatus,BestEdge,NewLevel,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge);
								 true->%else
 									Gesamtzustand = {NBasic,NewBranchEdges,NReject,NewStatus,BestEdge,NewLevel,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge},
									test(NBasic, Level, FragName, NodeX, FindCount, BestEdge,  MyNodeName,Gesamtzustand),
							        loop(NBasic,NewBranchEdges,NReject,NewStatus,BestEdge,NewLevel,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge)
								  end
  							  
							  
                       end
                 end,

  
        loop(NewBasicEdges,NewBranchEdges,RejectEdges,NewStatus,BestEdge,NewLevel,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge);
         

%%---------------------------accept----------------------------------------	
		
		{accept,Edge}  ->
	
        %X und Y vertauscht so das X der eigene Knoten ist und Y der knoten von dem die Nachricht gekommen ist
	    {Weight,NodeY,NodeX} = Edge,
		werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Empfangen: 'accept' vom Knoten "++werkzeug:to_String(NodeY)++"\r\n"),
        NewTestEdge = {},
        %Wenn das Gewicht der Kante, kleiner als das Gewicht meiner aktuellsten kleinsten bekanten Kante ist
            if Weight < BestWT->
  	 		    NewBestWT = Weight,
		        NewBestEdge = {Weight,NodeY};
	        true->
                NewBestWT = BestWT,
	            NewBestEdge = BestEdge
            end,
       report(BasicEdges, BranchEdges, RejectEdges, Status, NewBestEdge, Level, MyNodeName, FragName, InBranch, FindCount, NewBestWT, NewTestEdge);


%%--------------------------reject----------------------------------------		
		{reject,Edge}  ->

         %X und Y vertauscht so das X der eigene Knoten ist und Y der knoten von dem die Nachricht gekommen ist
	    {Weight,NodeY,NodeX} = Edge,
		werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Empfangen: 'reject' vom Knoten "++werkzeug:to_String(NodeY)++"\r\n"),
		 
        %Kante als rejected markieren (Wenn es eine Basic Kante ist), und Prozedur Test ausf�hren
        
        IsBasic = edgeManagment:isBasicEdge(Weight,BasicEdges),
           if IsBasic == true ->
	          [NBasic,NBranch] =  edgeManagment:basicToRejectEdge(Weight,BasicEdges,BranchEdges);
           IsBasic == false->
              NBasic = BasicEdges,
              NBranch = BranchEdges
            end,

	    Gesamtzustand = {NBasic,NBranch,RejectEdges,Status,BestEdge,Level,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge},
        test(NBasic,Level,FragName,NodeX,FindCount,BestEdge,MyNodeName, Gesamtzustand),
				 				 
        loop(NBasic,NBranch,RejectEdges,Status,BestEdge,Level,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge);


%%--------------------------report----------------------------------------		
	    {report,Weight,Edge} ->
			
	    
         %X und Y vertauscht so das X der eigene Knoten ist und Y der knoten von dem die Nachricht gekommen ist
	     {NWeight,NNodeY,NNodeX} = Edge,
          
         werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Empfangen: 'report' vom Knoten "++werkzeug:to_String(NNodeY)++", mit BestWT "++werkzeug:to_String(Weight)++"\r\n"),
   
         %Branch Kante
        {InBranchWeight,BNodeX,BNodeY} = InBranch,   
         %Test ob es die InBranch Kante ist, von der der Report kommt, wenn ja ist dieser Knoten nichtTeil des Cores
         if NWeight /=InBranchWeight ->
		    NewFindCount = FindCount-1,
	            %Wenn die �bergebene Kante kleiner ist als die derzeit gespeicherte beste kante, wird diese druch die neue ersetzt, ansonsten wird die alte behalten
	  			if Weight < BestWT ->
			        NewBestWT= Weight,
		            NewBestEdge= {NWeight,NNodeY,NNodeX};
		        true-> %else
				    NewBestWT=BestWT, 
		            NewBestEdge=BestEdge
		        end,
		     report(BasicEdges, BranchEdges, RejectEdges, Status, NewBestEdge, Level, MyNodeName, FragName, InBranch, NewFindCount, NewBestWT, TestEdge) ,    
	         loop(BasicEdges,BranchEdges,RejectEdges,Status,NewBestEdge,Level,MyNodeName,FragName,InBranch,NewFindCount,NewBestWT,TestEdge); 
	  
	       
		 true-> %else -> Knoten ist Teil des Cores
             %Wenn der Knoten noch im Status 'find' ist, wird der Report verz�gert, indem die Nachicht neu an sich selbst geschick wird 
	         if Status == find ->
		           timer:send_after(100,self(), {report,Weight,Edge});  %Zeitverz�gert da der Prozess sich sonst zu schnell zu viele Nachrichten schicken w�rde 
                Status /= find -> %else
                   if Weight > BestWT ->
                      [NBasic, NBranch]= changeroot(MyNodeName, BestEdge, BasicEdges, BranchEdges, Level),

			          loop(NBasic,NBranch,RejectEdges,Status,BestEdge,Level,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge);
                   true-> %else
                       if BestWT == 99999 -> %9999 als unendlich
					      stop(MyNodeName) ;
				       true->
				          true
			           end
                   end
               end,
             loop(BasicEdges,BranchEdges,RejectEdges,Status,BestEdge,Level,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge)
	     end;



		
%%---------------------------changeroot-----------------------------------		
		{changeroot,Edge} ->
		[NBasic, NBranch] = changeroot(MyNodeName, BestEdge, BasicEdges, BranchEdges, Level),
		loop(NBasic,NBranch,RejectEdges,Status,BestEdge,Level,MyNodeName,FragName,InBranch,FindCount,BestWT,TestEdge)
	end.

%%Prozeduren

%%---------------------wakeup--------------------------------------------------------------------------------------------------------

wakeup(BasicEdges,MyNodeName)->

%Kleinste Basic Kante w�hlen (Durch die Sortierte Liste, ist dies immer der HEAD)
[{EdgeWeight, NodeNameY}|Rest] = BasicEdges,

%connect senden
NodeID = global:whereis_name(NodeNameY),
NodeID !{connect,0,{EdgeWeight,MyNodeName,NodeNameY}}, 
werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Gesendet: 'Connect' an Node: "++werkzeug:to_String(NodeNameY)++"\r\n"),
werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Der Knoten "++werkzeug:to_String(MyNodeName)++" hat nun vollgende Branch Kanten: "++werkzeug:to_String({EdgeWeight, NodeNameY})++"\r\n"), 

%R�ckgabe der neuen Werte: BassicEdges, BranchKanten,Level,Status
{Rest,[{EdgeWeight, NodeNameY}],0,found}.


%%---------------------test--------------------------------------------------------------------------------------------------------------

%Mit dieser Funktion, wird an die kleinste Basic Kante, eine Test Nachricht gesendet. Falls es keine Basic Kanten gibt, wird die Prozedur report() aufgerufen

test([],L,F,NodeX,Counter,BestEdge,MyNodeName,Gesamtzustand)->
{BasicEdges,BranchEdges,RejectEdges,S,BestEdge,L,MyNodeName,F, NewInBranchEdge,FindCount,NewBestWT,TestEdge} = Gesamtzustand,
NewTestEdge = {},
NGesamtzustand = {BasicEdges,BranchEdges,RejectEdges,S,BestEdge,L,MyNodeName,F, NewInBranchEdge,FindCount,NewBestWT,NewTestEdge},%Dem urspr�nglichem GEsamtzustand, wird druch die leere TestEdge erg�nzt
report(NGesamtzustand); 


test([{BasicWeight,BasicNode}|Rest],L,F,NodeX,Counter,BestEdge,MyNodeName,Gesamtzustand)->
%zuweisung der TestEdge
TestEdge = {BasicWeight,NodeX,BasicNode},
	
%Test Nachricht senden
NodeID = global:whereis_name(BasicNode),
NodeID !{test,L,F, TestEdge},
werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Gesendet: 'test' an Node: "++werkzeug:to_String(BasicNode)++" \r\n"),
TestEdge. %TestEdge zur�ckgeben



%%---------------------report-----------------------------------------------------------------------------------------------------
%Report sendet wenn der findCounter 0 ist, seine kleinste ausgehende Basic kannte, an seine InBranch zur�ck
%Au�erdem hat sie als return Wert die Statusveraenderung auf 'found' !!!

report(Gesamtzustand)->
{BasicEdges,BranchEdges,RejectEdges,Status,BestEdge,Level,MyNodeName,FragName,{InBranchWeight,InBranchNodeX,InBranchNodeY},FindCount,BestWT,TestEdge} = Gesamtzustand,
report(BasicEdges,BranchEdges,RejectEdges,Status,BestEdge,Level,MyNodeName,FragName,{InBranchWeight,InBranchNodeX,InBranchNodeY},FindCount,BestWT,TestEdge).

report(BasicEdges,BranchEdges,RejectEdges,Status,BestEdge,Level,MyNodeName,FragName,{InBranchWeight,InBranchNodeX,InBranchNodeY},FindCount,BestWT,TestEdge) ->
	if FindCount == 0 andalso TestEdge == {}->
		   NewStatus = found,
		   %sende die beste gefunde Kante an die inBranch
	       NodeID = global:whereis_name(InBranchNodeY),
		   NodeID !{report,BestWT,{InBranchWeight,InBranchNodeX,InBranchNodeY}},
	       werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Gesendet: 'report' mit Best-WT: "++werkzeug:to_String(BestWT)++" an Node: "++werkzeug:to_String(InBranchNodeY)++"\r\n");

	   true ->
    NewStatus = Status
	   end,

	loop(BasicEdges,BranchEdges,RejectEdges,NewStatus,BestEdge,Level,MyNodeName,FragName,{InBranchWeight,InBranchNodeX,InBranchNodeY},FindCount,BestWT,TestEdge).

%%---------------------changeroot-------------------------------------------------------------------------------------------

changeroot(MyNodeName, BestEdge, BasicEdges, BranchEdges, Level) ->
	{Weight,NodeY} = BestEdge,
	Edge = {Weight, MyNodeName, NodeY},
	%{InBranchWeight,BNodeX,BNodeY} = InBranch, 
	
	IsBranch = edgeManagment:isBasicEdge(Weight,BranchEdges),
		if IsBranch == true ->
			NBasic = BasicEdges,
			NBranch = BranchEdges,
			NodeID = global:whereis_name(NodeY),
			NodeID ! {changeroot, Edge},
		    werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Gesendet: 'changeroot' vom Knoten "++werkzeug:to_String(MyNodeName)++"\r\n"),
			[NBasic, NBranch];
		true ->
			global:whereis_name(NodeY) ! {connect, Level, Edge}, 
			[NBasic,NBranch] =  edgeManagment:basicToBrunchEdge(Weight,BasicEdges,BranchEdges),
			werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Der Knoten "++werkzeug:to_String(MyNodeName)++" hat nun vollgende Branch Kanten: "++werkzeug:to_String(NBranch)++"\r\n"), 
		    werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Gesendet: 'connect' vom Knoten "++werkzeug:to_String(MyNodeName)++" zum anderen Fragment\r\n"),
			[NBasic, NBranch]
		end.
					

stop(MyNodeName) ->
werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"), werkzeug:timeMilliSecond()++" Testausgabe: Prozedur stop() aufgerufen \r\n").
%Hier muss warscheinlich wieder das loop aufgerufen werden  ***************************!!!
