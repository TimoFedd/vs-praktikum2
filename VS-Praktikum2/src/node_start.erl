-module(node_start).
-export([start/1]).
%TEST-----

% erzeuge Serverprozess
start(NodeName) ->
	
	%Namen der Config Datei bilden
	Config = string:concat(atom_to_list(NodeName), ".cfg"),
	
	%Config Datei auslesen und sortieren
	{ok, NodeEdges} = file:consult(Config),
	SortedNodeEdges = sortNodeEdges(NodeEdges,[]),

	%io:fwrite("NodeEdgesALT ~w \n",[NodeEdges]),    
	%io:fwrite("NodeEdgesNEU ~w \n",[SortedNodeEdges]),
	
	%alle Rechner duch Ping bekannt machen
    {ok,Hostlist} = file:consult("hosts.cfg"),
    net_adm:world_list(Hostlist),
	
    %Node Prozess starten    %Parameter: BasicEdges,BranchEdges,RejectEdges,Status,BestEdge,Level,Name,Fragmentname,InBrunc,FindCount ,BestWT,TestEdge 
	global:register_name(NodeName, spawn(node,loop, [SortedNodeEdges,[],[],sleeping,{},0,NodeName,NodeName,{},0,99999,{}])).  %Wie BestWT auf unendlich???

     %Zufälliges aufwecken
    %timer:send_after(random:uniform(20)*1000,global:whereis_name(node1), {wakeup}).



%%     werkzeug:logging('server.log', "Server: Wird gestartet... " ++ werkzeug:timeMilliSecond()++"\r\n" ),

% Sortiert die Kanten aufsteigend, kleinste steht links
sortNodeEdges([],SortedNodeEdges) -> SortedNodeEdges;

sortNodeEdges(NodeEdges,SortedNodeEdges) ->
	Min=lists:min(NodeEdges),
	NewNodeEdges=lists:delete(Min,NodeEdges),
	NewSortedNodeEdges=SortedNodeEdges++[Min],
		
	sortNodeEdges(NewNodeEdges, NewSortedNodeEdges). 



