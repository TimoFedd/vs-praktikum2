-module(nodeStarter).
-export([start/1]).

start(Anzahl) -> 
%{ok, NodesConfig} = file:consult("nodes.cfg"),
%{ok, AnzahlNodes} = werkzeug:get_config_value(anzahlNodes, NodesConfig),
starteNodes(Anzahl,0)
.

starteNodes(AnzahlNodes,AnzahlNodes)->io:format("Alle Nodes gestartet");
	
starteNodes(AnzahlNodes, GestarteteNodes)->
	NodeName=lists:concat(["node",GestarteteNodes+1]),
	node_start:start(list_to_atom(NodeName)),
	starteNodes(AnzahlNodes, GestarteteNodes+1).
