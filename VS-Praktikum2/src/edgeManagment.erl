-module(edgeManagment).
-export([basicToBrunchEdge/3,basicToRejectEdge/3,isBasicEdge/2,initToBrunch/8]).



%---------------------------------basicToBrunchEdge---------------------------------------------------------
%L�scht die �bergebene Kante aus der Liste der BasicEdges, und f�gt diese der BrunchListe hinzu, und gibt anschlie�end beide Listen zur�ck

basicToBrunchEdge(EdgeWeight,BasicEdges,BraunchEdges)->
	basicToBrunchEdge(EdgeWeight,BasicEdges,BraunchEdges,[],[]).

%Basic kante gefunden
basicToBrunchEdge(EdgeWeight,[{BasicWeight,BasicEdge}|Rest],BraunchEdges,NBasic,NBranch) when EdgeWeight == BasicWeight ->
	basicToBrunchEdge(EdgeWeight,Rest,BraunchEdges,NBasic,BraunchEdges++[{BasicWeight,BasicEdge}]);
	
%Druch die Liste gehen
basicToBrunchEdge(EdgeWeight,[{BasicWeight,BasicEdge}|Rest],BraunchEdges,NBasic,NBranch) when EdgeWeight /= BasicWeight ->
   basicToBrunchEdge(EdgeWeight,Rest,BraunchEdges,NBasic++[{BasicWeight,BasicEdge}],NBranch);
	
%Ende der Liste
basicToBrunchEdge(EdgeWeight,[],BraunchEdges,NBasic,NBranch) ->	
[NBasic,NBranch].


%---------------------------------basicToRejectEdge---------------------------------------------------------
%L�scht die �bergebene Kante aus der Liste der BasicEdges, und f�gt diese der RejectListe hinzu, und gibt anschlie�end beide Listen zur�ck

basicToRejectEdge(EdgeWeight,BasicEdges,RejectEdges)->
	basicToRejectEdge(EdgeWeight,BasicEdges,RejectEdges,[],[]).

%Basic kante gefunden
basicToRejectEdge(EdgeWeight,[{BasicWeight,BasicEdge}|Rest],RejectEdges,NBasic,NReject) when EdgeWeight == BasicWeight ->
	basicToRejectEdge(EdgeWeight,Rest,RejectEdges,NBasic,RejectEdges++[{BasicWeight,BasicEdge}]);
	
%Druch die Liste gehen
basicToRejectEdge(EdgeWeight,[{BasicWeight,BasicEdge}|Rest],RejectEdges,NBasic,NReject) when EdgeWeight /= BasicWeight ->
   basicToRejectEdge(EdgeWeight,Rest,RejectEdges,NBasic++[{BasicWeight,BasicEdge}],NReject);
	
%Ende der Liste
basicToRejectEdge(EdgeWeight,[],RejectEdges,NBasic,NReject) ->	
[NBasic,NReject].



%---------------------------------isBasicEdge---------------------------------------------------------

%liefert true zur�ck wenn die Kante eine Basic Kante ist, sonst false
isBasicEdge(Weight,[{BasicWeight,BasicEdge}|Rest]) ->
   if Weight == BasicWeight ->
	   true;
   Weight /= BasicWeight -> %else
       isBasicEdge(Weight,Rest)
   end;

isBasicEdge(Weight,[])->
false.


%-------------------------initToBrunch-----------------------------------------
%Diese Funktion sendet an alle Brunch kanten ein initiate, au�er an die InBranchKante. Und erh�ht gegebenfalls den Find Counter
initToBrunch(NodeX,[{BranchWeight,BrunchEdge}|Rest],InBranchWeight,FindCount,L,S,F,MyNodeName) ->
	
	
	if BranchWeight /= InBranchWeight ->
		   
		   %Sende ein initiate an die Brunch Kante
           werkzeug:logging(string:concat(atom_to_list(MyNodeName), ".log"),werkzeug:timeMilliSecond()++" Gesendet: initiate an knoten "++werkzeug:to_String(BrunchEdge)++"\r\n"), 
		   NID =global:whereis_name(BrunchEdge),
		   NID !{initiate,L,F,S,{BranchWeight,NodeX,BrunchEdge}},
	              
		          %Wenn der Status 'find' ist, wird der Counter um eins erhoeht, sonst nicht
		          if S == find ->
						 AddFindCount = FindCount+1;
					 S /= find ->
						 AddFindCount = FindCount
				 end,
		   %Naechste Kante auswaelen
	       initToBrunch(NodeX,Rest,InBranchWeight,AddFindCount,L,S,F,MyNodeName);
	%else              
	BranchWeight == InBranchWeight -> 
		
        %Die InBranch Kante wird uebersprungen
		initToBrunch(NodeX,Rest,InBranchWeight,FindCount,L,S,F,MyNodeName)
	   
    end;

 %wenn die Liste komplett durchgegangen ist:
 initToBrunch(NodeX,[],InBranchWeight,FindCount,L,S,F,MyNodeName)->
   
	%FindCount wird zurueck gegeben
	FindCount.

	